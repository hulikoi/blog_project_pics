package ictgradschool.project.servlets;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class UserDAO {
    Properties dbProps;
    ServletContext context;

    //constructor p
    public UserDAO(ServletContext context) {
        this.context = context;
        dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/dbProps.properties"))) {//
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }
    }

    /*todo make methods to connect to the db, get the user details for a login number.
    might need a separate method to check if a user exists first, return true or false, then if true, continue to get the user details, if false then tell servlet to re-direct to the login page and the login page displays message.
    todo also need a method to connect to db and create a new entry.
    */

    //create a user object from the db info and return that user to calling method.
    public User getUser(String userName) {
        User user = null;
        System.out.println("getUser method accessed");

        //initialise sql drivers
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_user_db WHERE user_id = ?")) {
                Stmt.setString(1, userName);
                System.out.println("aaddimg uname nto statemnet");

                System.out.println("about to exc block");
                try (ResultSet rs = Stmt.executeQuery()) {
                    //create user, assuming columns are in order id, name,location, img src string
                    System.out.println("resultset block");
                    int pwCol = rs.findColumn("passwords");
                    int fnameCol = rs.findColumn("user_fname");
                    int lnameCol= rs.findColumn("user_lname");
                    int countryCol=rs.findColumn("country");
                    int dateCol=rs.findColumn("date_of_birth");
                    int bioCol=rs.findColumn("bio");
                    int pic_urlCol=rs.findColumn("profil_pic_url");
                    int iteraCol=rs.findColumn("iterations");
                    int saltCol=rs.findColumn("salt");
                    while (rs.next()) {

                        String passwords = rs.getString(pwCol);
                        String user_fname = rs.getString(fnameCol);
                        String user_lname = rs.getString(lnameCol);
                        String country = rs.getString(countryCol);
                        java.sql.Date date_of_birth = rs.getDate(dateCol);
                        String bio = rs.getString(bioCol);
                        String profile_pic_url = rs.getString(pic_urlCol);
                        Integer iteration = rs.getInt(iteraCol);
                        byte[] salt = rs.getBytes(saltCol);



                        user = new User(userName, user_lname, user_fname, country, profile_pic_url, passwords, bio, salt, iteration, date_of_birth);
                        System.out.println("got user " + user.getFname()+"\n"+ user.getLname()+"\n"+user.getDob()+"\n"+user.getPassword()+"\n"+user.getUserName()+"\n"+user.getBio()+"\n"+user.getCountry()+"\n"+user.getUrl()+"\n"+user.getIteration()+"\n"+user.getSalt());
                    }
                    return user;
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return user;
    }

    public void addUser(User user) {
        //Use 'getUser' to check and get if any existing user in the db.
        // If so, we do not go on
//        User existingUser= getUser(user.getUserName()+"");
//        if(existingUser!=null){
//            return;
//        }
        //get db connection
        // try to add new user
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("connected for new user");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO group_project_user_db (user_id, passwords, user_fname, user_lname, country,date_of_birth, bio, profil_pic_url, salt, iterations) VALUES (?,?,?,?,?,?,?,?,?,?)")) {
                Stmt.setString(1, user.getUserName());
                Stmt.setString(2, user.getPassword());
                Stmt.setString(3, user.getFname());
                Stmt.setString(4, user.getLname());
                Stmt.setString(5, user.getCountry());
                Stmt.setDate(6, user.getDob());
                Stmt.setString(7, user.getBio());
                Stmt.setString(8, user.getUrl());
                Stmt.setBytes(9, user.getSalt());
                Stmt.setInt(10, user.getIteration());

                //execute update
                Stmt.execute();
                System.out.println("executed");
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }

    }

    public void addMemory(User user, String memory) {

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO corny_strings (user_id,input) VALUES (?,?)")) {
                Stmt.setString(1, user.getUserName());
                Stmt.setString(2, memory);


                //execute update
                Stmt.execute();
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }

    }

    public void addPicURL(User user) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected - go for pic transfer");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_user_db SET profil_pic_url =? WHERE user_id = ?;")) {
                Stmt.setString(1, user.getUrl());
                Stmt.setString(2, user.getUserName());

                //execute update
                Stmt.execute();
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }

    }

    protected void modifyUser(User user){


        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected - go for account changing");
            if(user.getPassword()==null){
                try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_user_db SET user_fname=?,user_lname=?,country=?,profil_pic_url=?,bio=?,date_of_birth=? WHERE user_id = ?")) {
                    Stmt.setString(1,user.getFname());
                    Stmt.setString(2,user.getLname());
                    Stmt.setString(3,user.getCountry());
                    Stmt.setString(4,user.getUrl());
                    Stmt.setString(5,user.getBio());
                    Stmt.setDate(6,user.getDob());
                    Stmt.setString(7, user.getUserName());

                    //execute update
                    Stmt.execute();
                }
            } else {
                try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_user_db SET user_fname=?,user_lname=?,country=?,profil_pic_url=?,bio=?,date_of_birth=?,passwords=?,salt=?,iterations=? WHERE user_id = ?")) {
                    Stmt.setString(1,user.getFname());
                    Stmt.setString(2,user.getLname());
                    Stmt.setString(3,user.getCountry());
                    Stmt.setString(4,user.getUrl());
                    Stmt.setString(5,user.getBio());
                    Stmt.setDate(6,user.getDob());
                    Stmt.setString(7,user.getPassword());
                    Stmt.setBytes(8,user.getSalt());
                    Stmt.setInt(9,user.getIteration());
                    Stmt.setString(10, user.getUserName());

                    //execute update
                    Stmt.execute();
                }
            }
            //prepare the query to get the user info

        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
    }


    public void delUser(String username) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected - go for account deletion");
            try (PreparedStatement Stmt = conn.prepareStatement("DELETE FROM group_project_user_db WHERE user_id = ?;")) {
                Stmt.setString(1, username);
                //execute update
                Stmt.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
