package ictgradschool.project.servlets;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class addByte {

    public static void main(String[] args) {


        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("C:\\Users\\dsten\\IdeaProjects\\blog_project_secondary\\web\\WEB-INF\\dbProps.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        byte[] byteArr = Passwords.getNextSalt(10);
        String password=Passwords.base64Encode(Passwords.hash("a".toCharArray(),byteArr,10));
        System.out.println(password);

        //get db connection
        try (
                Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_user_db SET salt =?,passwords=? WHERE user_id = ?;")) {
                Stmt.setBytes(1,byteArr );
                Stmt.setString(2,password);
                Stmt.setString(3, "next");


                //execute update
                Stmt.execute();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}



