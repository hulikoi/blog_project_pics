package ictgradschool.project.servlets;

import java.io.Serializable;
import java.sql.Date;

public class User implements Serializable {
    /* todo complete the user object with appropriate instance variables, constructors and getters/setters.
    variables that may be needed:
     login number, lname, country, list of memories/gratitudes, picture url.
    * */

    private String userName, lname, fname, country, url, password;
    private String bio;
    private byte[] salt;
    private int iteration;
    java.sql.Date dob;



    public User() {
    }

    public User(String userName, String lname, String fname, String country, String url, String bio, Date dob) {
        this.userName = userName;
        this.lname = lname;
        this.fname = fname;
        this.country = country;
        this.url = url;
        this.bio = bio;
        this.dob = dob;
    }

    public User(String userName, String lname, String fname, String country, String url, String password, String bio, byte[] salt, int iteration, java.sql.Date dob) {
        this.userName = userName;
        this.lname = lname;
        this.fname = fname;
        this.country = country;
        this.url = url;
        this.password = password;
        this.bio = bio;
        this.salt = salt;
        this.iteration = iteration;
        this.dob=dob;
    }

    public java.sql.Date getDob() {
        return dob;
    }

    public void setDob(java.sql.Date dob) {
        this.dob = dob;
    }

    public User(String userName, String lname, String fname, String country, String url, String password, String bio, java.sql.Date dob) {
        this.userName = userName;
        this.lname = lname;
        this.fname = fname;
        this.country = country;
        this.url = url;
        this.password = password;
        this.bio = bio;
        this.salt = salt;
        this.iteration = iteration;
        this.dob=dob;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public int getIteration() {
        return iteration;
    }

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
