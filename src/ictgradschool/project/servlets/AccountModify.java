package ictgradschool.project.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AccountModify extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //get values from form
        String username = request.getParameter("username");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String country = request.getParameter("country");
        String bio = request.getParameter("bio");
        String picurl = request.getParameter("picurl");
        String dobString = request.getParameter("dob");

        //convert date to sql format
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date javaDob = null;
        try {
            javaDob = format.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.sql.Date sqlDob = new java.sql.Date(javaDob.getTime());


        //create a bse64encoded version of salted,hashed,randomly iterated password
        //if passwordfeild not empty then regen password
        byte[] salt = null;
        int iteration = 0;
        String pass = null;
        User newUser = null;
        if (request.getParameter("password").length() != 0) {
            salt = Passwords.getNextSalt();
            iteration = (int) (Math.random() * 100000) + 1000000;
            pass = Passwords.base64Encode(Passwords.hash(request.getParameter("password").toCharArray(), salt, iteration));
            newUser = new User(username, lname, fname, country, picurl, pass, bio, salt, iteration, sqlDob);
        } else {
            newUser = new User(username, lname, fname, country, picurl, bio, sqlDob);
        }
        //create new user object with these details

        // create dao
        UserDAO dao = new UserDAO(request.getServletContext());
        //call addUser method of dao and pass user object
        dao.modifyUser(newUser);
        //create session, not sure why, do we need both the request and the session to contain the user? anuyway, forward to article creation pg
        HttpSession session = request.getSession();
        session.setAttribute("user", newUser);
        request.setAttribute("user", newUser);
        request.getRequestDispatcher("ArticleCreate.jsp").forward(request, response);
    }

}

