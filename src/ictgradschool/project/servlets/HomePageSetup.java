package ictgradschool.project.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


@WebServlet(name = "HomePageSetup")
public class HomePageSetup extends HttpServlet {


        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            System.out.println("The adding article servlet");


            HttpSession session = request.getSession();
            ArticleDAO dao3 = new ArticleDAO(getServletContext());
            ArrayList<Article> HParticles = dao3.getHPArticles();
            System.out.println(HParticles.get(0).getTitle());
            request.setAttribute("HParticles", HParticles);
            dao3.getHPArticles ();


            request.getRequestDispatcher("HomePage.jsp").forward(request,response);
        }

    }






