package ictgradschool.project.servlets;

import java.io.Serializable;
import java.security.Timestamp;

public class Comment implements Serializable {
/*TODO
* While I remember. we will need to add:
* a list to store child comments,
* a toString method that generates html to display the comment,
* the toString will  need to call the toString of child comments.
* this will be recursive, base case will be when there are no more children
* then the toString will write the closing html.
* I don't actually know how this will work yet. so don't worry about it.
* The jsp page will then call the toString of the parent
* comment and this will write out all of the children as well.
* */
    private int commentID,comment_to,atricle_id;
    private String  comment,comment_BY;
    private Timestamp comment_time;

    public int getAtricle_id() {
        return atricle_id;
    }

    public void setAtricle_id(int atricle_id) {
        this.atricle_id = atricle_id;
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public int getComment_to() {
        return comment_to;
    }

    public void setComment_to(int comment_to) {
        this.comment_to = comment_to;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment_BY() {
        return comment_BY;
    }

    public void setComment_BY(String comment_BY) {
        this.comment_BY = comment_BY;
    }

    public Timestamp getComment_time() {
        return comment_time;
    }

    public void setComment_time(Timestamp comment_time) {
        this.comment_time = comment_time;
    }

    public Comment(int commentID, int comment_to, int atricle_id, String comment, String comment_BY, Timestamp comment_time) {
        this.commentID = commentID;
        this.comment_to = comment_to;
        this.atricle_id = atricle_id;
        this.comment = comment;
        this.comment_BY = comment_BY;
        this.comment_time = comment_time;
    }

    public Comment(){}
}
