package ictgradschool.project.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;



@WebServlet(name = "SingleArticleServlet")
public class SingleArticleServlet extends HttpServlet{


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("The adding article servlet");
        System.out.println("ID:" +request.getParameter("articleID"));
        int articleID = Integer.parseInt(request.getParameter("articleID"));

        HttpSession session = request.getSession();
        ArticleDAO dao = new ArticleDAO(getServletContext());
        Article article = dao.getArticleByID(articleID);
        System.out.println("Got article");
        request.setAttribute("article", article);

        ArrayList<Comment>comment = dao.getCommentByArticleID(articleID);
        System.out.println("Got comment");
        request.setAttribute("comment", comment);
        request.setAttribute("articleID", articleID);




        request.getRequestDispatcher("SingleArticleView.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}






