package ictgradschool.project.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet(name = "ArticleMaker")
public class ArticleMaker extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("The adding article servlet");

        String title = request.getParameter("title");
        String content = request.getParameter("content");

        System.out.println("title: " + title);
        System.out.println("content: " + content);

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        ArticleDAO dao = new ArticleDAO(getServletContext());
        dao.addArticle(title, user, content);

        //tidied this up, instead of declaring a new array list
        // getting a dao etc, this is all handled by the get articles by user method below.
        request.setAttribute("articles", getArticlesByUser(user));
        request.getRequestDispatcher("ArticleView.jsp").forward(request, response);
    }

    //used by the login servlet as landing after successful login. line 51. Also called by delete link buttons.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        //checking if get req is for deleting an article
        if (request.getParameter("delArticleID") != null) {
            System.out.println("delComment value is: " + request.getParameter("delArticleID"));
            int articleID = Integer.parseInt(request.getParameter("delArticleID"));
            String username = request.getParameter("username");
            if (!(new ArticleDAO(getServletContext()).delArticle(articleID, username))) {
                request.setAttribute("denied", "denied");
            }
        }
        request.setAttribute("articles", getArticlesByUser(user));
        request.getRequestDispatcher("ArticleView.jsp").forward(request, response);
    }

    private ArrayList<Article> getArticlesByUser(User user) {
        ArticleDAO dao = new ArticleDAO(getServletContext());
        return dao.getArticles(user);
    }
}
