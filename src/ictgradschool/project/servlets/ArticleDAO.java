package ictgradschool.project.servlets;

import sun.misc.UCEncoder;

import javax.servlet.ServletContext;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class ArticleDAO {
    Properties dbProps;
    ServletContext context;

    //constructor p
    public ArticleDAO(ServletContext context) {
        this.context = context;
        dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/dbProps.properties"))) {//
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addArticle(String title, User user, String content) {

//        Article article = null;
        System.out.println("addArticle method accessed");

        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO group_project_article(title, user_id, content) VALUES (?,?,?)")) {
                Stmt.setString(1, title);
                Stmt.setString(2, user.getUserName());
                Stmt.setString(3, content);


                Stmt.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public ArrayList<Article> getArticles(User user) {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        System.out.println("getArticle method accessed");

        //initialise sql drivers
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article WHERE user_id= ?")) {
                Stmt.setString(1, user.getUserName());

                System.out.println("about to exc block");
                try (ResultSet rs = Stmt.executeQuery()) {
                    //create user, assuming columns are in order id, name,location, img src string
                    System.out.println("resultset block");
                    int titleCol = rs.findColumn("title");
                    int userCol = rs.findColumn("user_id");
                    int contentCol = rs.findColumn("content");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(user.getUserName());
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        System.out.println("got article ");
                        articlelist.add(article);
                    }
                    return articlelist;
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return articlelist;
    }


    public ArrayList<Article> getHPArticles() {
        Article article = null;
        ArrayList<Article> articlelist = new ArrayList<>();
        System.out.println("getHPArticle method accessed");

        //initialise sql drivers
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected(getHP)");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article")) {


                System.out.println("about to exc block(HP)");
                try (ResultSet rs = Stmt.executeQuery()) {
                    //create user, assuming columns are in order id, name,location, img src string
                    System.out.println("resultset block");
                    int titleCol = rs.findColumn("title");
                    int userCol = rs.findColumn("user_id");
                    int contentCol = rs.findColumn("content");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(rs.getString(userCol));
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        System.out.println("got article (HP)");
                        articlelist.add(article);
                    }
                    return articlelist;
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return articlelist;
    }

    public Article getArticleByID(int id) {
        System.out.println("getArticleByID method accessed");
        Article article = null;
        //initialise sql drivers
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_article WHERE articel_id= ?")) {

                Stmt.setInt(1, id);

                System.out.println("about to exc block");
                try (ResultSet rs = Stmt.executeQuery()) {
                    //create user, assuming columns are in order id, name,location, img src string
                    System.out.println("resultset block");
                    int titleCol = rs.findColumn("title");
                    int userCol = rs.findColumn("user_id");
                    int contentCol = rs.findColumn("content");
                    int publishtimeCol = rs.findColumn("publish_time");
                    while (rs.next()) {
                        article = new Article();
                        article.setArticleID(rs.getInt(1));
                        article.setUser_id(rs.getString(userCol));
                        article.setTitle(rs.getString(titleCol));
                        article.setContent(rs.getString(contentCol));
                        System.out.println("got article ");
                    }
                    return article;
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return article;
    }


    public ArrayList<Comment> getCommentByArticleID(int articleID) {

        Comment comment = null;
        ArrayList<Comment> commentList = new ArrayList<>();
        System.out.println("getComment method accessed");

        //initialise sql drivers
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_comment WHERE articel_id= ?")) {
                Stmt.setInt(1, articleID);

                System.out.println("about to exc block");
                try (ResultSet rs = Stmt.executeQuery()) {
                    //create user, assuming columns are in order id, name,location, img src string
                    System.out.println("resultset block");

                    int userCol = rs.findColumn("made_by");
                    int commentCol = rs.findColumn("comment");
                    while (rs.next()) {
                        comment = new Comment();
                        comment.setCommentID(rs.getInt(1));

                        comment.setComment(rs.getString(commentCol));

                        comment.setComment_BY(rs.getString(userCol));
                        System.out.println("got comment ");
                        commentList.add(comment);
                    }
                    return commentList;
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return commentList;


    }

    public void replaceArticle(int id, String title, String content) {
        System.out.println("getArticleByID method accessed");
        Article article = null;
        //initialise sql drivers
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_article SET content = ?, title = ? WHERE articel_id= ? ")) {

                Stmt.setString(1, content);
                Stmt.setString(2, title);
                Stmt.setInt(3, id);

                Stmt.executeUpdate();


            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
    }

    //method for checking if user is allowed to delete a given article. called by the delArticle() method below
    public boolean canDeleteArticle(int articleID,String username) {
        System.out.println("canDeleteArticle method accessed");
        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected - go for checking permission to del article");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT DISTINCT user_id FROM group_project_article AS articles WHERE articles.articel_id = ?;")) {
                Stmt.setInt(1, articleID);

                System.out.println("about to exc block");
                try (ResultSet rs = Stmt.executeQuery()) {
                    System.out.println("resultset block");
                    int articleUserNameCol = rs.findColumn("user_id");
                    while (rs.next()) {
                        if ((rs.getString(articleUserNameCol).equals(username))) {
                            System.out.println("can del");
                            return true;
                        } else {
                            System.out.println("denied del");
                            return false;
                        }
                    }
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return false;
    }


    public boolean delArticle(int articleID,String username) {
        if (canDeleteArticle(articleID,username)) {
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                System.out.println("yep we are connected - go for account deletion");
                try (PreparedStatement Stmt = conn.prepareStatement("DELETE FROM group_project_article WHERE articel_id = ?;")) {
                    Stmt.setInt(1, articleID);
                    //execute update
                    Stmt.execute();
                    System.out.println("deletion successful");
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("deletion not successful");
            return false;
        }
        return false;
    }

}


