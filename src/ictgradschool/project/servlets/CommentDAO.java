package ictgradschool.project.servlets;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class CommentDAO {
    Properties dbProps;
    ServletContext context;

    //constructor p
    public CommentDAO(ServletContext context) {
        this.context = context;
        dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/dbProps.properties"))) {//
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }
    }

    public void addComment(Comment comment, User user) {
//, Timestamp comment_time, int comment_to
//        Article article = null;
        System.out.println("addComment method accessed");

        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected -ready to add comment");
            //prepare the query to get the user info


            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO group_project_comment(comment, made_by,articel_id) VALUES (?,?,?)")) {
//                , comment_time, comment_to


                Stmt.setString(1, comment.getComment());
                Stmt.setString(2, user.getUserName());
                Stmt.setInt(3, comment.getAtricle_id());

//                Stmt.setInt(3, comment_to);
//                Stmt.setTimestamp(4, comment_time);


                Stmt.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean canDeleteComment(int commentID,int articleID,String username) {
        System.out.println("canDeleteComment method accessed");
        //get db connection
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("yep we are connected - go for checking permission to del comment");
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT DISTINCT user_id,made_by FROM group_project_article AS articles,group_project_comment AS comments WHERE comments.comment_id = ? AND comments.articel_id = articles.articel_id")) {
                Stmt.setInt(1, commentID);

                System.out.println("about to exc block");
                try (ResultSet rs = Stmt.executeQuery()) {
                    System.out.println("resultset block");
                    int commentUserNameCol = rs.findColumn("made_by");
                    int articleUserNameCol = rs.findColumn("user_id");
                    while (rs.next()) {
                        if (rs.getString(commentUserNameCol).equals(username) || (rs.getString(articleUserNameCol).equals(rs.getString(username)))) {
                            System.out.println("can del");
                            return true;
                        } else {
                            System.out.println("denied del");
                            return false;
                        }
                    }
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return false;
    }


    public boolean delComment(int commentID,int articleID,String username) {
        if (canDeleteComment(commentID,articleID,username)) {
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                System.out.println("yep we are connected - go for account deletion");
                try (PreparedStatement Stmt = conn.prepareStatement("DELETE FROM group_project_comment WHERE comment_id = ?;")) {
                    Stmt.setInt(1, commentID);
                    //execute update
                    Stmt.execute();
                    System.out.println("deletion successful");
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("deletion not successful");
            return false;
        }
        return false;
    }

}
