package ictgradschool.project.servlets;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class UploadServlet extends HttpServlet {
    private File uploadfolder, tempfolder;

    @Override
    public void init() throws ServletException {
        super.init();
        //check upload folder exists, if not create it
        this.uploadfolder = new File(getServletContext().getRealPath("/Photos"));
        if (!uploadfolder.exists()) {
            uploadfolder.mkdir();
        }

        //create temp upload folder
        this.tempfolder = new File(getServletContext().getRealPath("/WEB-INF/temp"));
        if (!tempfolder.exists()) {
            tempfolder.mkdir();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        PrintWriter out = response.getWriter();
        //check if adding a default pic
        if (request.getParameter("defaultPic") != null) {
            HttpSession session = request.getSession();

            User user = (User) session.getAttribute("user");
            if (user == null) {
                out.println("oops no user");
            }

            user.setUrl(request.getParameter("dropdown"));

            UserDAO userDAO = new UserDAO(getServletContext());
            userDAO.addPicURL(user);

            response.sendRedirect("UserPage.jsp");

        } else {

            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(4 * 1024);
            factory.setRepository(tempfolder);
            ServletFileUpload upload = new ServletFileUpload(factory);

            HttpSession session = request.getSession();
//what happens if user==null? create new user.
            User user = (User) session.getAttribute("user");

            response.setContentType("text/html");

            if (user == null) {
                out.println("oops no user");
            }

            try {
                List<FileItem> fileItems = upload.parseRequest(request);
                File fullsizeImageFile = null;

                for (FileItem fi : fileItems
                ) {
                    if (!fi.isFormField() && (fi.getContentType().equals("image/png") || fi.getContentType().equals("image/jpeg"))) {
                        String filename = fi.getName();
                        fullsizeImageFile = new File(uploadfolder, filename);
                        fi.write(fullsizeImageFile);
                    }
                }
                user.setUrl("Photos/" + fullsizeImageFile.getName());

                UserDAO userDAO = new UserDAO(getServletContext());
                userDAO.addPicURL(user);

                response.sendRedirect("UserPage.jsp");

                out.println("<img src=../Photos/" + fullsizeImageFile.getName() + " width=\"300\">");

            } catch (Exception e) {
                throw new ServletException(e);
            }
        }
    }


}
