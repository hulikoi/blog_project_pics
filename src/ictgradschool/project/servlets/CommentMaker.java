package ictgradschool.project.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Timestamp;

public class CommentMaker extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("The adding comment servlet");


//        int conmment_to = request.getParameter("comment_to");


//            String content = request.getParameter("content");

//        System.out.println("comment: " + comment);
//            System.out.println("content: " + content);

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Comment comment=new Comment();

        comment.setComment( request.getParameter("comment"));
        comment.setAtricle_id(Integer.parseInt(request.getParameter("articleID")));
        comment.setComment_BY(request.getParameter("comment_BY"));
        comment.setComment_to(Integer.parseInt(request.getParameter("comment_to")));
//        int comment_to = Integer.parseInt(request.getParameter("comment_to"));
//        Timestamp comment_time=request.getParameter("comment_time");
        CommentDAO dao = new CommentDAO(getServletContext());
        dao.addComment(comment,user);
//,comment_time,comment_to
        System.out.println("com: "+comment.getAtricle_id() + " resp: " +request.getParameter("articleID"));

        request.setAttribute("articleID",comment.getAtricle_id());

        request.getRequestDispatcher("SingleArticleServlet").forward(request,response);
    }

    //get method does comment delete, returns user to page, on failure attaches a denied attribute
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("delComment value is: "+request.getParameter("delCommentID"));
        int commentID =Integer.parseInt(request.getParameter("delCommentID"));
        int articleID =Integer.parseInt(request.getParameter("articleID"));
        String username =request.getParameter("username");
        System.out.println("deleting comment "+commentID);

        CommentDAO dao = new CommentDAO(getServletContext());
        if (dao.delComment(commentID,articleID,username)) {
            request.setAttribute("articleID",articleID);
            request.getRequestDispatcher("SingleArticleServlet").forward(request, response);
        } else {
            request.setAttribute("denied","denied");
            request.getRequestDispatcher("SingleArticleServlet").forward(request, response);
        }

    }
}
