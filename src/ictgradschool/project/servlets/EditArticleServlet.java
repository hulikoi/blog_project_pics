package ictgradschool.project.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;



@WebServlet(name = "EditArticleMaker")
public class EditArticleServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("The adding article servlet");
        System.out.println("ID:" +request.getParameter("articleID"));
        int articleID = Integer.parseInt(request.getParameter("articleID"));

        HttpSession session = request.getSession();
        ArticleDAO dao = new ArticleDAO(getServletContext());
        Article article = dao.getArticleByID(articleID);
        System.out.println("Got article");
        request.setAttribute("article", article);



        request.getRequestDispatcher("EditArticle.jsp").forward(request,response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("The adding article servlet");


        String title = request.getParameter("title");
        String content = request.getParameter("content");
        int articleID = Integer.parseInt(request.getParameter("articleID"));

        System.out.println("title: " + title);
        System.out.println("content: " + content);

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        ArticleDAO dao = new ArticleDAO(getServletContext());
        dao.replaceArticle(articleID, title, content);

        //looks like this was getting a list of articles but then directing to the single article page.
        //which is it? I changed to go to the article view page instead.
//        ArticleDAO dao2 = new ArticleDAO(getServletContext());
        ArrayList<Article> articles = dao.getArticles(user);
        System.out.println(articles.get(0).getTitle());
        request.setAttribute("articles", articles);
        dao.getArticles (user);

        System.out.println("Did we get here?");
//        request.setAttribute("articleID", articleID);
        request.getRequestDispatcher("ArticleView.jsp").forward(request,response);
    }


}
