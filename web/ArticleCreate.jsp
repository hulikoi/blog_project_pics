<%--
  Created by IntelliJ IDEA.
  User: dwc1
  Date: 25/01/2019
  Time: 3:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">

        <title>Article Creation</title>

        <script src="ckeditor/ckeditor.js"></script>

        <style>
            #page {
                align-content: center;
                text-align: center;
            }


        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-md fixed-top" style="position: sticky">
            <div class="navbar-brand" style="padding-left: 150px"><a href="HomePageSetup" class="navbar-left" ><img src="BLOGO_t.png" ></a></div>
            <nav class="collapse navbar-collapse" style="background: white">

                <h1 class="navbar-nav mr-auto">
                    <a class="text-dark " href="HomePageSetup" style="text-decoration: none">BLOG-LOG-O.G</a>
                </h1>

                <div class="form-inline my-2 my-lg-0" style="padding-top: 23px ">
                    <div style="padding-right: 100px">
                        <p>${HParticles.user_id}</p>
                    </div>
                </div>
            </nav></nav>

        <div id="page">

            <form action="ArticleMaker" method="post">

                <input style="outline: none" type="text" name="title" placeholder="Title"/>
                <br>

                <label>
                    <textarea type="html" name="content"></textarea>
                </label>

                <script>
                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.
                    CKEDITOR.replace('content');
                </script>

                <button class="btn btn-outline-secondary  btn-sm" type="submit">Submit</button>
            </form>

        </div>

    </body>
</html>
