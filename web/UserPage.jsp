<%--
  Created by IntelliJ IDEA.
  User: hf57
  Date: 17/01/2019
  Time: 2:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>User Profile</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
          crossorigin="anonymous">
    <style>
        img {
            float: left;
        }

        ol li {
            list-style-type: none;

        }

        form label {
            display: inline-block;
            line-height: 1.8;
            vertical-align: top;
        }
    </style>
    <%--move functions to external file.--%>
    <script type="text/javascript">
        function switchImage() {
            // var image = document.getElementById("imageToSwap");
            var dropd = document.getElementById("dlist");
            var picurl = document.getElementById("picurl")
            // image.src = dropd.value;
            picurl.value = dropd.value;
        };
    </script>
    <%--captcha script--%>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<jsp:include page="nav.jsp"/>
<div class="container-fluid">
    <h1>User Account</h1>
    <br>
    <br>
    <%--ADDING FORM FOR UPLOADING USER PIC--%>
    <img src="${user.url}" width="225">
    <%--<img id="imageToSwap" src="Photos/Placeholder_Image.jpg" width="225">--%>
    <div>
        <h4>Choose Default picture:</h4>
        <form action="UploadServlet" method="post">
            <select id="dlist" onchange="switchImage()" name="dropdown">
                <option value="Photos/Placeholder_Image.jpg">Default</option>
                <option value="Photos/Batman_Profile_Pic.jpg">Batman</option>
                <option value="Photos/Wonder_Woman_Profile_Pic.jpg">Wonder Woman</option>
            </select>
            <%--TODO this button doesn't work as expected. currently sets user picture back to the placeholder pic--%>
            <input type="submit" class="btn btn-outline-secondary  btn-sm" value="Select Default" name="defaultPic">
        </form>
        <h4>Or upload your own:</h4>
        <form action="UploadServlet" method="post" enctype="multipart/form-data">
            <input class="btn btn-outline-secondary  btn-sm" type="file" name="userpic" size="50"/>
            <br><br>
            <input type="submit" class="btn btn-outline-secondary  btn-sm" value="Upload Pic">
        </form>
    </div>

    <br><br><br><br>
    <div class=col-sm-4 style="border: black">

        <form action="<core:out value="${user eq null ? 'AccountCreation': 'AccountModify'}"/>" method="POST">
            <input type="hidden" id="picurl" name="picurl" value="Photos/Placeholder_Image.jpg">
            <ol>
                <li>
                    <label for="username">Username</label>
                    <core:choose>
                        <core:when test="${user!=null}">
                            <input type="text" id="displayusername" placeholder="user name" name="username"
                                   value="${user.userName}" disabled>
                            <input type="hidden" id="username" placeholder="user name" name="username"
                                   value="${user.userName}">
                            <%--<core:out value="${user eq null ? '': 'disabled'}"/>--%>
                        </core:when>
                        <core:otherwise>
                            <input type="text" id="username" placeholder="user name" name="username"
                                   value="${user.userName}">
                        </core:otherwise>
                    </core:choose>
                </li>
                <li>
                    <label for="password">Password</label>
                    <input type="text" id="password" placeholder="password" name="password">
                </li>
                <li>
                    <label for="fname">First name</label>
                    <input type="text" id="fname" placeholder="first" name="fname" value="${user.fname}">
                </li>
                <li>
                    <label for="lname">Last name</label>
                    <input type="text" id="lname" placeholder="last" name="lname" value="${user.lname}">
                </li>
                <li>
                    <label for="dob">Dob</label>
                    <input type="date" id="dob" max="2010-01-01" name="dob" value="1940-07-13">
                </li>
                <li>
                    <label for="country">From</label>
                    <input type="text" id="country" placeholder="country" name="country" value="${user.country}">
                </li>
                <li>
                    <label for="bio">Bio</label>
                    <textarea id="bio" placeholder="Bio" name="bio">${user.bio}</textarea>
                </li>
            </ol>
            <hr>
            <%--captcha box--%>
            <core:if test="${user==null}">
            <div class="g-recaptcha" data-sitekey="6LcqHo4UAAAAABxffzmNF0QeLrlaVzJhtKlUvTot" data-callback="recaptchaCallback"></div>
            </core:if>
            <input class="btn btn-outline-secondary  btn-sm" type="submit" style="<core:out value="${user eq null ? 'visibility: hidden': ''}"/>" id="create" value="<core:out value="${user eq null ? 'Create': 'Update'}"/>">
            <hr>
        </form>
        <script>
            function recaptchaCallback() {
                var btnSubmit = document.getElementById("create");
                btnSubmit.setAttribute("style","");
            }
        </script>
        <core:if test="${user!=null}">
            <form action="AccountCreation" method="get" onsubmit="return confirm('Are you sure you want to delete your profile. This is total and irreversible');">
                <input type="hidden" id="deluser" name="username" value="${user.userName}">
                <input class="btn btn-outline-secondary  btn-sm" type="submit" id="delete" name="delete" value="delete">

            </form>
            <form action="Login" method="post">
                <input class="btn btn-outline-secondary  btn-sm" type="submit" id="logout" name="logout" value="logout">
            </form>
        </core:if>

    </div>


</div>
</div>
</body>
</html>