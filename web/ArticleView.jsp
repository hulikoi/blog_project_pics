<%@ page import="ictgradschool.project.servlets.Article" %>
<%@ page import="ictgradschool.project.servlets.ArticleDAO" %><%--
  Created by IntelliJ IDEA.
  User: dwc1
  Date: 25/01/2019
  Time: 3:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html>
    <head>
        <title>Article view</title>
    </head>
    <body>
        <jsp:include page="nav.jsp"/>


        <div class="container ">
            <core:forEach items="${articles}" var="articles">
                <div class="card" style="width: 90%;">

                    <div class="card-body">

                            <%--Article title links to article--%>
                        <a href="SingleArticleServlet?articleID=${articles.articleID}" class="text-dark"
                           id="hp_article_title1"><h5 class="card-title">${articles.title} </h5>

                        </a><a href="url"
                                                                                                       class="text-dark">
                        <h6 id="hp_author1">${articles.user_id}</h6></a>
                        <p class="card-text" id="hp_article_content1">${articles.publish_time}</p>
                        <a href="SingleArticleServlet?articleID=${articles.articleID}"
                           class="btn btn-outline-secondary  btn-sm">View Article</a>

                                <a href="EditArticleServlet?articleID=${articles.articleID}"
                                   class="btn btn-outline-secondary  btn-sm">Edit</a>
                                <core:if test="${user!=null && (user.userName == articles.user_id)}">
                                    <a href="ArticleMaker?delArticleID=${articles.articleID}&username=${user.userName}"
                                       class="btn btn-outline-secondary  btn-sm">Delete</a>
                                </core:if>
                    </div>
                </div>
            </core:forEach>
        </div>

        <hr>


    </body>
</html>
