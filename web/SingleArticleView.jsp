<%@ page import="ictgradschool.project.servlets.Article" %>
<%@ page import="ictgradschool.project.servlets.ArticleDAO" %><%--
  Created by IntelliJ IDEA.
  User: dwc1
  Date: 25/01/2019
  Time: 3:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<html>
    <head>

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">
        <title>Article view</title>
    </head>
    <body>
        <%--<jsp:include page="nav.jsp"/>--%>

            <nav class="navbar navbar-expand-md fixed-top" style="position: sticky">
                <div class="navbar-brand" style="padding-left: 150px"><a href="HomePageSetup" class="navbar-left" ><img src="BLOGO_t.png" ></a></div>
                <nav class="collapse navbar-collapse" style="background: white">

                    <h1 class="navbar-nav mr-auto">
                        <a class="text-dark " href="HomePageSetup" style="text-decoration: none">BLOG-LOG-O.G</a>
                    </h1>

                    <%--<form class="form-inline my-2 my-lg-0" action="Login" method="post">--%>

                    <%--</form>--%>
                </nav></nav>

<div class="container">

                <h3>${article.title}</h3>
                <h6>${article.user_id}</h6>
                <p>${article.publish_time}</p>
                ${article.content}
    <core:if test="${user!=null && (user.userName == article.user_id)}">
        <a href="ArticleMaker?delArticleID=${article.articleID}&username=${user.userName}"
           class="btn btn-outline-secondary  btn-sm">Delete</a>
    </core:if>
            <hr>



        <core:forEach items="${comment}" var="comment">
    <div class="">
            <div class="">
                <h4>Comment by: ${comment.comment_BY}</h4>
                <p>${comment.comment}</p>
                <core:if test="${user!=null && (user.userName == comment.comment_BY || user.userName==article.user_id)}">
                <form action="CommentMaker" method="get">
                    <input type="hidden" value="${article.articleID}" name="articleID">
                    <input type="hidden" value="${user.userName}" name="username">
                    <button type="submit" id="delComment" name="delCommentID" value="${comment.commentID}">Del Comment</button>
                </form>
                </core:if>
            </div>
            </div>
            <hr>
        </core:forEach>

        <form action="CommentMaker" method="post">
            <input type="text" id="comment" placeholder="enter comment" name="comment">
            <input type="hidden" id="connectToArticle" name="articleID" value="${article.articleID}">
            <input type="hidden" id="comment_BY" name="comment_BY" value="${user.userName}">
            <input type="hidden" id="comment_to" name="comment_to" value="0">
            <input type="submit" value="Comment">

        </form>
</div>
    </body>
</html>
